using System.Threading.Tasks;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data.BehaviorTest
{
    public class TestReadSession : IReadSession
    {
        public ValueTask DisposeAsync()
        {
            throw new System.NotImplementedException();
        }

        public IBlobReadRepository GetBlobReadRepository()
        {
            throw new System.NotImplementedException();
        }

        public IPublicationReadRepository GetPublicationReadRepository()
        {
            throw new System.NotImplementedException();
        }

        public ITagReadRepository GetTagReadRepository()
        {
            throw new System.NotImplementedException();
        }

        public ISourceReadRepository GetSourceReadRepository()
        {
            throw new System.NotImplementedException();
        }
    }
}
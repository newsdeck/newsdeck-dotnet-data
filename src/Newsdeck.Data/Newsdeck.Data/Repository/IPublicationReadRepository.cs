using Newsdeck.Data.Entity;

namespace Newsdeck.Data.Repository
{
    /// <summary>
    /// Publication read repository
    /// <see cref="IBaseReadRepository{T, TL}"/>
    /// </summary>
    public interface IPublicationReadRepository 
        : IBaseReadRepository<IPublicationEntity, long>
    {
        
    }
}
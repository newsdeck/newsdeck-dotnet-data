using System;
using System.Linq.Expressions;

namespace Newsdeck.Data.Filter
{
    /// <summary>
    /// Contains filter
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ContainsFilter<T> : PropertyFilter<T>
    {
        /// <summary>
        /// Expose the filter value
        /// </summary>
        public object Value => _value;
        
        private readonly object _value;

        /// <summary>
        /// Default constructor for ContainsFilter
        /// </summary>
        /// <param name="navigationPropertyPath">Access to the property</param>
        /// <param name="value">Filter value</param>
        public ContainsFilter(Expression<Func<T, object>> navigationPropertyPath,
            object value)
            : base(navigationPropertyPath)
        {
            _value = value;
        }
    }
}
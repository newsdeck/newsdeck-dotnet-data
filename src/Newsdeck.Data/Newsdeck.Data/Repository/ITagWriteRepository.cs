using Newsdeck.Data.Entity;
using Newsdeck.Data.Model;

namespace Newsdeck.Data.Repository
{
    /// <summary>
    /// Tag write repository
    /// </summary>
    public interface ITagWriteRepository
        : IBaseWriteRepository<ITagEntity, int, ICreateTagModel, IEditTagModel>
    {
        
    }
}
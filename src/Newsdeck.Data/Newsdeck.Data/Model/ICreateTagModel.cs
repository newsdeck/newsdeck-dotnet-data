using Newsdeck.Data.Data;

namespace Newsdeck.Data.Model
{
    /// <summary>
    /// Create tag entity model
    /// </summary>
    public interface ICreateTagModel : ICreateBaseModel, ITagData
    {
        
    }
}
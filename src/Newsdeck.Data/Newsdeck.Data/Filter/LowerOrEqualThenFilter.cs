using System;
using System.Linq.Expressions;

namespace Newsdeck.Data.Filter
{
    /// <summary>
    /// Lower or equal filter
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class LowerOrEqualThenFilter<T> : PropertyFilter<T>
    {
        /// <summary>
        /// Expose the filter value
        /// </summary>
        public IComparable Value => _value;
        private readonly IComparable _value;

        /// <summary>
        /// Default constructor for LowerOrEqualThenFilter
        /// </summary>
        /// <param name="navigationPropertyPath">Access to the property</param>
        /// <param name="value">Filter value</param>
        public LowerOrEqualThenFilter(Expression<Func<T, object>> navigationPropertyPath,
            IComparable value)
            : base(navigationPropertyPath)
        {
            _value = value;
        }
    }
}
namespace Newsdeck.Data.Data
{
    /// <summary>
    /// Publication data
    /// </summary>
    public interface IPublicationData
    {
        /// <summary>
        /// Json data
        /// Contains data not used for dataset querying
        /// but used later on in business stack
        /// </summary>
        string JsonData { get; }
        
        /// <summary>
        /// Source entity identifier
        /// </summary>
        long SourceIdentifier { get; }
        
        /// <summary>
        /// Publication cover blob entity identifier
        /// </summary>
        long? PublicationCoverIdentifier { get; }
        
        /// <summary>
        /// Original guid
        /// unique identifier for the publication
        /// </summary>
        string OriginalGuid { get; }
        
        /// <summary>
        /// Publication date unix time
        /// </summary>
        long PublicationDateUnixTime { get; }
    }
}
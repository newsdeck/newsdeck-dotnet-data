using System;
using System.Linq.Expressions;

namespace Newsdeck.Data.Filter
{
    /// <summary>
    /// Equal filter
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EqualFilter<T> : PropertyFilter<T>
    {
        /// <summary>
        /// Expose the filter value
        /// </summary>
        public object Value => _value;
        
        private readonly object _value;
        
        /// <summary>
        /// Default constructor for EqualFilter
        /// </summary>
        /// <param name="navigationPropertyPath">Access to the property</param>
        /// <param name="value">Filter value</param>
        public EqualFilter(Expression<Func<T, object>> navigationPropertyPath,
            object value)
            : base(navigationPropertyPath)
        {
            _value = value;
        }
    }
}
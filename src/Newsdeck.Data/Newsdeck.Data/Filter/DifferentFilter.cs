using System;
using System.Linq.Expressions;

namespace Newsdeck.Data.Filter
{
    /// <summary>
    /// Different filter
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DifferentFilter<T> : PropertyFilter<T>
    {
        /// <summary>
        /// Expose the filter value
        /// </summary>
        public object Value => _value;
        private readonly object _value;

        /// <summary>
        /// Default constructor for DifferentFilter
        /// </summary>
        /// <param name="navigationPropertyPath">Access to the property</param>
        /// <param name="value">Filter value</param>
        public DifferentFilter(Expression<Func<T, object>> navigationPropertyPath,
            object value)
            : base(navigationPropertyPath)
        {
            _value = value;
        }
    }
}
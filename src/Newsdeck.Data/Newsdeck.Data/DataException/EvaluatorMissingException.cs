using System;
using Newsdeck.Data.Filter;

namespace Newsdeck.Data.DataException
{
    /// <summary>
    /// EvaluatorMissingException is an exception
    /// representing a missing evaluator
    ///
    /// Usually this is Exception is thrown evaluating
    /// <see cref="IFilter"/> and <see cref="IOrder"/> instance within
    /// <see cref="IDataResult{TContract, TIdentifier}"/> implementation
    ///
    /// A usual fix is to make the IDataResult also implements
    /// <see cref="IFilterEvaluator{T,TContract}"/>
    /// and call the <see cref="FilterEvaluatorExtension.RegisterBase{T, TContract}"/> method
    /// This will enforce to implemented basic filters and orders methods
    ///
    /// If the Type is a custom implementation, you shall need to add the evaluator yourself,
    /// You could check implementation packages for examples
    /// </summary>
    [Serializable]
    public class EvaluatorMissingException : Exception
    {
        /// <summary>
        /// Evaluated full Type
        /// </summary>
        public Type EvaluatingType { get; }
        
        /// <summary>
        /// Default constructor providing only the accessed type
        /// </summary>
        /// <param name="type"></param>
        public EvaluatorMissingException(Type type)
            : base($"Evaluator is missing for type '{type.FullName}'")
        {
            EvaluatingType = type;
        }
    }
}
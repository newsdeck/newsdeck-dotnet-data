using System.Collections.Generic;

namespace Newsdeck.Data.Filter
{
    /// <summary>
    /// And filter
    /// </summary>
    public class AndFilter : IFilter
    {
        /// <summary>
        /// Expose a list of contained filters
        /// </summary>
        public IEnumerable<IFilter> Filters => _filters;

        private readonly List<IFilter> _filters;

        /// <summary>
        /// Default constructor, receive a list of IFilter implementation
        /// </summary>
        /// <param name="filters"></param>
        public AndFilter(params IFilter[] filters)
        {
            _filters = new List<IFilter>(filters);
        }
    }
}
namespace Newsdeck.Data.Data
{
    /// <summary>
    /// Tag data
    /// </summary>
    public interface ITagData
    {
        /// <summary>
        /// Tag label
        /// </summary>
        string Label { get; }
    }
}
using System;
using System.Linq.Expressions;

namespace Newsdeck.Data.Filter
{
    /// <summary>
    /// Greater or equal filter
    /// </summary>
    public class GreaterOrEqualThenFilter<T> : PropertyFilter<T>
    {
        /// <summary>
        /// Expose the filter value
        /// </summary>
        public IComparable Value => _value;

        private readonly IComparable _value;

        /// <summary>
        /// Default constructor for GreaterOrEqualThenFilter
        /// </summary>
        /// <param name="navigationPropertyPath">Access to the property</param>
        /// <param name="value">Filter value</param>
        public GreaterOrEqualThenFilter(Expression<Func<T, object>> navigationPropertyPath,
            IComparable value)
            : base(navigationPropertyPath)
        {
            _value = value;
        }
    }
}
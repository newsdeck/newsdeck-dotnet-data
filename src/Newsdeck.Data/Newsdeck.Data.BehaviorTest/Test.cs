using System.Threading.Tasks;
using Newsdeck.Data.Model;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data.BehaviorTest
{
    public class Test
    {
        private class Model : ICreateBlobModel
        {
            
        }
        
        public static async Task Run()
        {
            var provider = new TestNewsdeckSessionProvider();
            var session = provider.OpenWriteSession();
            var repository = session.GetBlobWriteRepository();
            var r = await repository.CreateAsync(new Model());
            await session.CommitAsync();
        }
    }
}
namespace Newsdeck.Data.BehaviorTest
{
    public class TestNewsdeckSessionProvider : INewsdeckSessionProvider
    {
        public IReadSession OpenReadSession()
        {
            return new TestReadSession();
        }

        public IWriteSession OpenWriteSession()
        {
            return new TestWriteSession();
        }
    }
}
using Newsdeck.Data.Data;

namespace Newsdeck.Data.Model
{
    /// <summary>
    /// Create source entity model
    /// </summary>
    public interface ICreateSourceModel : ICreateBaseModel, ISourceData
    {
        
    }
}
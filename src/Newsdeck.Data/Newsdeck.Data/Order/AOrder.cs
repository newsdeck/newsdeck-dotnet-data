using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Newsdeck.Data.Order
{
    /// <summary>
    /// Abstract class for property orders
    /// Expose the PropertyInfo for order consumption
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class AOrder<T> : IOrder
    {
        /// <summary>
        /// Expose the <see cref="PropertyInfo"/> in {T} targeted by the order
        /// </summary>
        public PropertyInfo PropertyInfo { get; }
        
        /// <summary>
        /// protected constructor for AOrder
        /// Providing an expression accessing a property from {T},
        /// the constructor fill the <see cref="PropertyInfo"/> 
        /// </summary>
        /// <param name="navigationPropertyPath"></param>
        protected AOrder(Expression<Func<T, object>> navigationPropertyPath)
        {
            PropertyInfo = navigationPropertyPath.GetPropertyInfo();
        }
    }
}
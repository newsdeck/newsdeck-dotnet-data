using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data.Entity;
using Newsdeck.Data.Model;

namespace Newsdeck.Data.Repository
{
    /// <summary>
    /// Source write repository
    /// </summary>
    public interface ISourceWriteRepository
        : IBaseWriteRepository<ISourceEntity, long, ICreateSourceModel, IEditSourceModel>
    {
        /// <summary>
        /// Add an url to the given source
        /// Source may hold multiple input url
        ///
        /// e.g: source with minified urls, etc. 
        /// </summary>
        /// <param name="sourceIdentifier"></param>
        /// <param name="url"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task AddUrlToSource(long sourceIdentifier, string url, CancellationToken cancellationToken);
    }
}
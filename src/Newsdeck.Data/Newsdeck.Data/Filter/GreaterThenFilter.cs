using System;
using System.Linq.Expressions;

namespace Newsdeck.Data.Filter
{
    /// <summary>
    /// Greater then filter
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GreaterThenFilter<T> : PropertyFilter<T>
    {
        /// <summary>
        /// Expose the filter value
        /// </summary>
        public IComparable Value => _value;
        private readonly IComparable _value;

        /// <summary>
        /// Default constructor for GreaterThenFilter
        /// </summary>
        /// <param name="navigationPropertyPath">Access to the property</param>
        /// <param name="value">Filter value</param>
        public GreaterThenFilter(Expression<Func<T, object>> navigationPropertyPath,
            IComparable value)
            : base(navigationPropertyPath)
        {
            _value = value;
        }
    }
}
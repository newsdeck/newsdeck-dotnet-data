using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data.Entity;

namespace Newsdeck.Data.Repository
{
    /// <summary>
    /// Source read repository
    /// <see cref="IBaseReadRepository{T, TL}"/>
    /// </summary>
    public interface ISourceReadRepository
        : IBaseReadRepository<ISourceEntity, long>
    {
        /// <summary>
        /// Search a single source using an input (url, name, etc)
        /// The implementation shall use the input to retrieve the best matching source
        /// </summary>
        /// <param name="input"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<ISourceEntity> FindMatchingSource(string input, CancellationToken cancellationToken);
    }
}
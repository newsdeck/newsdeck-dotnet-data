using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data.Entity;
using Newsdeck.Data.Model;

namespace Newsdeck.Data.Repository
{
    /// <summary>
    /// Publication write repository
    /// </summary>
    public interface IPublicationWriteRepository
        : IBaseWriteRepository<IPublicationEntity, long, ICreatePublicationModel, IEditPublicationModel>
    {
        /// <summary>
        /// Delete multiple source entity
        /// provide the source identifier to prevent undesired deletion
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="sourceIdentifier"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task DeleteAsync(IEnumerable<long> identifier, long sourceIdentifier, CancellationToken cancellationToken);


        /// <summary>
        /// Add a link to a given tag identifier
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="tagIdentifier"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task LinkPublicationToTagAsync(long identifier, int tagIdentifier, CancellationToken cancellationToken);
        
        /// <summary>
        /// Add link to the given tag identifiers
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="tagIdentifier"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task RemoveLinkPublicationToTagAsync(long identifier, int tagIdentifier, CancellationToken cancellationToken);

        /// <summary>
        /// Remove link between publication and given tag
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="tagIdentifiers"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task LinkPublicationToTagsAsync(long identifier, IEnumerable<int> tagIdentifiers,
            CancellationToken cancellationToken);

        /// <summary>
        /// Remove link between publication and given tags
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="tagIdentifiers"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task RemoveLinksPublicationToTagAsync(long identifier, IEnumerable<int> tagIdentifiers,
            CancellationToken cancellationToken);
    }
}
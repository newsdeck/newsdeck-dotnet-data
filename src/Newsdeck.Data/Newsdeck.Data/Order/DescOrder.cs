using System;
using System.Linq.Expressions;

namespace Newsdeck.Data.Order
{
    /// <summary>
    /// Desc order
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DescOrder<T> : AOrder<T>
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="navigationPropertyPath">Access to the property</param>
        public DescOrder(Expression<Func<T, object>> navigationPropertyPath)
            : base(navigationPropertyPath)
        {
        }
    }
}
using Newsdeck.Data.Order;

namespace Newsdeck.Data.Filter
{
    /// <summary>
    /// Extensions methods for <see cref="IFilterEvaluator{T,TContract}"/> contract
    /// </summary>
    public static class FilterEvaluatorExtension
    {
        /// <summary>
        /// Call all <see cref="IFilterEvaluator{T,TContract}.RegisterFilterEvaluator{TFilter}"/> and
        /// <see cref="IFilterEvaluator{T,TContract}.RegisterOrderEvaluator{TOrder}"/>
        /// for the default existing <see cref="IFilter"/> and <see cref="IOrder"/> implementation in
        /// the Newsdeck.Data project.
        ///
        /// This is usually called in the constructor of the
        /// <see cref="IFilterEvaluator{T,TContract}"/> implementation
        /// </summary>
        /// <param name="evaluator"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TContract"></typeparam>
        public static void RegisterBase<T, TContract>(this IFilterEvaluator<T, TContract> evaluator)
        {
            evaluator.RegisterFilterEvaluator<EqualFilter<TContract>>(evaluator.EvaluateEqualFilter);
            evaluator.RegisterFilterEvaluator<DifferentFilter<TContract>>(evaluator.EvaluateNotEqualFilter);
            evaluator.RegisterFilterEvaluator<InFilter<TContract>>(evaluator.EvaluateIn);
            evaluator.RegisterFilterEvaluator<GreaterThenFilter<TContract>>(evaluator.EvaluateGreaterThenFilter);
            evaluator.RegisterFilterEvaluator<GreaterOrEqualThenFilter<TContract>>(evaluator.EvaluateGreaterOrEqualThenFilter);
            evaluator.RegisterFilterEvaluator<LowerThenFilter<TContract>>(evaluator.EvaluateLowerThenFilter);
            evaluator.RegisterFilterEvaluator<LowerOrEqualThenFilter<TContract>>(evaluator.EvaluateLowerOrEqualThenFilter);
            
            evaluator.RegisterFilterEvaluator<AndFilter>(evaluator.EvaluateAnd);
            
            evaluator.RegisterOrderEvaluator<DescOrder<TContract>>(evaluator.EvaluateDescOrder);
            evaluator.RegisterOrderEvaluator<AscOrder<TContract>>(evaluator.EvaluateAscOrder);
            
        }
    }
}
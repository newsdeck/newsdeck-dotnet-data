namespace Newsdeck.Data.Data
{
    /// <summary>
    /// Source data
    /// </summary>
    public interface ISourceData
    {
        /// <summary>
        /// Source type
        /// used later in business to retrieve which consumer
        /// can execute the source
        /// </summary>
        string Type { get; }
        
        /// <summary>
        /// Json data
        /// Contains data not used for dataset querying
        /// but used later on in business stack
        /// </summary>
        string JsonData { get; }

        /// <summary>
        /// Store the next check date in unix time
        /// </summary>
        long NextCheckAfterUnixTime { get; }
        
        /// <summary>
        /// Store the last time changed unix time
        /// </summary>
        long LastTimeChangedUnixTime { get; }
        
        /// <summary>
        /// Blob icon identifier
        /// </summary>
        long? SourceIconIdentifier { get; }
    }
}
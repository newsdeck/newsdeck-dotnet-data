using System.Collections.Generic;
using Newsdeck.Data.Data;

namespace Newsdeck.Data.Entity
{
    /// <summary>
    /// Publication entity
    /// </summary>
    public interface IPublicationEntity : IBaseEntity<long>, IPublicationData
    {
        /// <summary>
        /// Linq access to PublicationCover from <see cref="IPublicationData.PublicationCoverIdentifier"/>
        /// </summary>
        IBlobEntity PublicationCover { get; }
        
        /// <summary>
        /// Linq access to Source from <see cref="IPublicationData.SourceIdentifier"/>
        /// </summary>
        ISourceEntity Source { get; }
        
        /// <summary>
        /// Linq access to Tags
        /// We let the data implementation layer the choice of implementation
        /// Tag addition/removal is managed from the write repository
        /// </summary>
        IEnumerable<ITagEntity> Tags { get; }
    }
}
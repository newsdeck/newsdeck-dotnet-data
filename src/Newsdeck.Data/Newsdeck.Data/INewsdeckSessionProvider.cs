namespace Newsdeck.Data
{
    /// <summary>
    /// Represent the father contract to be implemented by every data provider
    /// 
    /// Default use case will be create a class implementing this interface,
    /// then by design, every necessary methods/class will have to be implemented
    ///
    /// Data consumers (business, services, etc.) will then have to instantiate
    /// the implementation (using dependency injection, or any other methods) to
    /// access read/write repositories, ensuring data state
    /// </summary>
    public interface INewsdeckSessionProvider
    {
        /// <summary>
        /// Open a read session as an IReadSession
        /// </summary>
        /// <returns>instance of <see cref="IReadSession"/></returns>
        IReadSession OpenReadSession();
        
        /// <summary>
        /// Open a write session as an IWriteSession
        /// </summary>
        /// <returns>instance of <see cref="IWriteSession"/></returns>
        IWriteSession OpenWriteSession();
    }
}
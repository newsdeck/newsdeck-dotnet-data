namespace Newsdeck.Data.TestGeneric
{
    public class BlobTest
    {
        public async Task TestBlob()
        {
            const int msWaiting = 3000;
            
            CancellationToken cancellationToken = CancellationToken.None;
            var instance = Instance.Get();
            var sessionProvider = instance.GetInstance<INewsdeckSessionProvider>();

            IBlobEntity createdEntity;
            IBlobEntity editedEntity;

            await using (var readSession = sessionProvider.OpenWriteSession())
            {
                var blobReadRepository = readSession.GetBlobWriteRepository();
                var id = await blobReadRepository.CreateAsync(new CreateBlobModel(), cancellationToken);
                createdEntity = await blobReadRepository.GetWithIdAsync(id, cancellationToken);
                await readSession.CommitAsync(cancellationToken);
            }

            Thread.Sleep(msWaiting);
            
            await using (var readSession2 = sessionProvider.OpenWriteSession())
            {
                var blobReadRepository2 = readSession2.GetBlobWriteRepository();
                var edit = new EditBlobModel();
                await blobReadRepository2.EditAsync(createdEntity.Identifier, edit, cancellationToken);
                editedEntity = await blobReadRepository2.GetWithIdAsync(createdEntity.Identifier, cancellationToken);
                await readSession2.CommitAsync(cancellationToken);
            }

            var all = await GetAll();
            
            Assert.GreaterOrEqual(editedEntity.LastModificationUnixTime, editedEntity.CreationUnixTime + msWaiting);
            Assert.GreaterOrEqual(editedEntity.LastModificationUnixTime, createdEntity.LastModificationUnixTime + msWaiting);
        }
    }
}
namespace Newsdeck.Data.Model
{
    /// <summary>
    /// Create base model
    /// Contains all basic data for entity creation
    /// </summary>
    public interface ICreateBaseModel
    {
        
    }
}
using System;
using System.Linq.Expressions;

namespace Newsdeck.Data.Filter
{
    /// <summary>
    /// Start with filter
    /// </summary>
    public class StartWithFilter<T> : PropertyFilter<T>
    {
        /// <summary>
        /// Expose the filter value
        /// </summary>
        public string Value => _value;
        private readonly string _value;

        /// <summary>
        /// Default constructor for StartWithFilter
        /// </summary>
        /// <param name="navigationPropertyPath">Access to the property</param>
        /// <param name="value">Filter value</param>
        public StartWithFilter(Expression<Func<T, object>> navigationPropertyPath,
            string value)
            : base(navigationPropertyPath)
        {
            _value = value;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Newsdeck.Data.Filter
{
    /// <summary>
    /// In filter
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class InFilter<T> : PropertyFilter<T>
    {

        /// <summary>
        /// Expose the filter values in a list
        /// </summary>
        public IEnumerable<object> Values => _inValue;
        
        private readonly List<object> _inValue; 
        
        /// <summary>
        /// Default constructor for InFilter
        /// </summary>
        /// <param name="navigationPropertyPath">Access to the property</param>
        /// <param name="inValue">Filter values</param>
        public InFilter(Expression<Func<T, object>> navigationPropertyPath, IEnumerable<object> inValue) 
            : base(navigationPropertyPath)
        {
            _inValue = inValue.ToList();
        }
    }
}
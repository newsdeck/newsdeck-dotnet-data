using Newsdeck.Data.Entity;

namespace Newsdeck.Data.Repository
{
    /// <summary>
    /// Blob read repository
    /// <see cref="IBaseReadRepository{T, TL}"/>
    /// </summary>
    public interface IBlobReadRepository 
        : IBaseReadRepository<IBlobEntity, long>
    {
        
    }
}
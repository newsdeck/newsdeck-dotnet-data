using Newsdeck.Data.Data;

namespace Newsdeck.Data.Model
{
    /// <summary>
    /// Create blob entity model
    /// </summary>
    public interface ICreateBlobModel : ICreateBaseModel, IBlobData
    {
        
    }
}
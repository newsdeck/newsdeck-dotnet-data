namespace Newsdeck.Data.Entity
{
    /// <summary>
    /// Base entity
    /// Include all basic field an Entity should include
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBaseEntity<out T>
    {
        /// <summary>
        /// Identifier key
        /// </summary>
        T Identifier { get; }
        
        /// <summary>
        /// Creation unix time
        /// </summary>
        long CreationUnixTime { get; }

        /// <summary>
        /// Last modification unix time
        /// </summary>
        long LastModificationUnixTime { get; }

    }
}
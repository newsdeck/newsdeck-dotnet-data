using Newsdeck.Data.Data;

namespace Newsdeck.Data.Model
{
    /// <summary>
    /// Edit source entity model
    /// </summary>
    public interface IEditSourceModel : IEditBaseModel, ISourceData
    {
        
    }
}
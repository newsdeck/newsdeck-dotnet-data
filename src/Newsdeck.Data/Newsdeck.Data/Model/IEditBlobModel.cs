using Newsdeck.Data.Data;

namespace Newsdeck.Data.Model
{
    /// <summary>
    /// Edit blob entity model
    /// </summary>
    public interface IEditBlobModel : IEditBaseModel, IBlobData
    {
        
    }
}
using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Newsdeck.Data.Filter
{
    /// <summary>
    /// Abstract class for property filters
    /// Expose the PropertyInfo for filter consumption
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class PropertyFilter<T> : IFilter
    {
        /// <summary>
        /// Expose the <see cref="PropertyInfo"/> in {T} targeted by the filter
        /// </summary>
        public PropertyInfo PropertyInfo { get; }
        
        /// <summary>
        /// protected constructor for PropertyFilter
        /// Providing an expression accessing a property from {T}
        /// the constructor fill the <see cref="PropertyInfo"/> 
        /// </summary>
        /// <param name="navigationPropertyPath"></param>
        protected PropertyFilter(Expression<Func<T, object>> navigationPropertyPath)
        {
            PropertyInfo = navigationPropertyPath.GetPropertyInfo();
        }
    }
}
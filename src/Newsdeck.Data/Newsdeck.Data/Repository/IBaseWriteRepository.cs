using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data.Entity;

namespace Newsdeck.Data.Repository
{
    /// <summary>
    /// Base write repository
    /// Holds basic methods for every write repository
    /// </summary>
    /// <typeparam name="TContract"></typeparam>
    /// <typeparam name="TIdentifier"></typeparam>
    /// <typeparam name="TCreate"></typeparam>
    /// <typeparam name="TEdit"></typeparam>
    public interface IBaseWriteRepository<TContract, TIdentifier, in TCreate, in TEdit> 
        : IBaseReadRepository<TContract, TIdentifier> 
        where TContract : IBaseEntity<TIdentifier>
    {
        /// <summary>
        /// current <see cref="IWriteSession"/> access 
        /// </summary>
        new IWriteSession Session { get; }
        
        /// <summary>
        /// Create an entity using the model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<TIdentifier> CreateAsync(TCreate model, CancellationToken cancellationToken);
        
        /// <summary>
        /// Edit the given entity using the model
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task EditAsync(TIdentifier identifier, TEdit model, CancellationToken cancellationToken);
        
        /// <summary>
        /// Delete a given entity
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task DeleteAsync(TIdentifier identifier, CancellationToken cancellationToken);
    }
}
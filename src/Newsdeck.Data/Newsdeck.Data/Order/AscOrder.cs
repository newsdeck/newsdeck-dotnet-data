using System;
using System.Linq.Expressions;

namespace Newsdeck.Data.Order
{
    /// <summary>
    /// Asc order
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class AscOrder<T> : AOrder<T>
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="navigationPropertyPath">Access to the property</param>
        public AscOrder(Expression<Func<T, object>> navigationPropertyPath)
            : base(navigationPropertyPath)
        {
        }
    }
}
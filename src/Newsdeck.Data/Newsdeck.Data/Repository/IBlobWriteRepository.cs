using Newsdeck.Data.Entity;
using Newsdeck.Data.Model;

namespace Newsdeck.Data.Repository
{
    /// <summary>
    /// Blob write repository
    /// </summary>
    public interface IBlobWriteRepository
        : IBaseWriteRepository<IBlobEntity, long, ICreateBlobModel, IEditBlobModel>
    {
        
    }
}
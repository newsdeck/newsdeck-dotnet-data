using Newsdeck.Data.Data;

namespace Newsdeck.Data.Model
{
    /// <summary>
    /// Edit tag entity model
    /// </summary>
    public interface IEditTagModel : IEditBaseModel, ITagData
    {
        
    }
}
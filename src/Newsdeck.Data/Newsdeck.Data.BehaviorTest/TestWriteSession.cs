using System.Threading.Tasks;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data.BehaviorTest
{
    public class TestWriteSession : TestReadSession, IWriteSession
    {
        public new async ValueTask DisposeAsync()
        {
            await CommitAsync();
            await base.DisposeAsync();
        }
        
        public IBlobWriteRepository GetBlobWriteRepository()
        {
            return new TestBlobWriteRepository(this);
        }

        public IPublicationWriteRepository GetPublicationWriteRepository()
        {
            throw new System.NotImplementedException();
        }

        public ITagWriteRepository GetTagWriteRepository()
        {
            throw new System.NotImplementedException();
        }

        public ISourceWriteRepository GetSourceWriteRepository()
        {
            throw new System.NotImplementedException();
        }

        public Task CommitAsync()
        {
            throw new System.NotImplementedException();
        }

        public Task RollbackAsync()
        {
            throw new System.NotImplementedException();
        }
    }
}
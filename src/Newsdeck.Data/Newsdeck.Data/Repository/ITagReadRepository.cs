using Newsdeck.Data.Entity;

namespace Newsdeck.Data.Repository
{
    /// <summary>
    /// Tag read repository
    /// <see cref="IBaseReadRepository{T, TL}"/>
    /// </summary>
    public interface ITagReadRepository
        : IBaseReadRepository<ITagEntity, int>
    {
        
    }
}
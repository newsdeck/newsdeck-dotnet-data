using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data.Entity;

namespace Newsdeck.Data
{
    /// <summary>
    /// Data representation result
    /// Returned from all read methods allowing the consumer to add filters, orders, limits and other specific
    /// data manipulation before retrieving the actual data using <see cref="AsListAsync"/>
    /// </summary>
    /// <typeparam name="TContract"></typeparam>
    /// <typeparam name="TIdentifier"></typeparam>
    public interface IDataResult<TContract, TIdentifier> where TContract : IBaseEntity<TIdentifier>
    {
        /// <summary>
        /// Consume all the requests to retrieve a list of data asynchronously 
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<List<TContract>> AsListAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Add an instance of <see cref="IFilter"/> to the data result context
        /// </summary>
        /// <param name="filter"></param>
        void AddFilter(IFilter filter);

        /// <summary>
        /// Add a limit to the data result context
        /// </summary>
        /// <param name="limit"></param>
        void SetLimit(int? limit);

        /// <summary>
        /// Add an instance of <see cref="IOrder"/> to the data result context
        /// </summary>
        /// <param name="order"></param>
        void AddOrder(IOrder order);
    }
}
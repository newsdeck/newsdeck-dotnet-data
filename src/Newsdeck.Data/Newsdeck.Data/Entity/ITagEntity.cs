using Newsdeck.Data.Data;

namespace Newsdeck.Data.Entity
{
    /// <summary>
    /// Tag entity
    /// </summary>
    public interface ITagEntity : IBaseEntity<int>, ITagData
    {

    }
}
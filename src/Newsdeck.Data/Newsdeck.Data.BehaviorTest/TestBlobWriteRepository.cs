using System;
using System.Threading.Tasks;
using Newsdeck.Data.Entity;
using Newsdeck.Data.Model;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data.BehaviorTest
{
    public class TestBlobWriteRepository : TestBlobReadRepository, IBlobWriteRepository
    {
        private readonly TestWriteSession _session;

        public TestBlobWriteRepository(TestWriteSession session) : base(session)
        {
            _session = session;
        }

        public IWriteSession Session => _session;

        public async Task<long> CreateAsync(ICreateBlobModel model)
        {
            long? r = null;
            return r ?? throw new Exception();
        }

        public Task EditAsync(long identifier, IEditBlobModel model)
        {
            throw new System.NotImplementedException();
        }
    }
}
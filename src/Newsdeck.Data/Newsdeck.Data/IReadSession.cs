using System;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data
{
    /// <summary>
    /// Represents a read session for our dataset
    /// No methods exposed by those repository can alter the data, therefore right is limited
    /// Expose methods to retrieve read repositories
    /// </summary>
    public interface IReadSession : IAsyncDisposable
    {
        /// <summary>
        /// Get a blob repository from the session 
        /// </summary>
        /// <returns>An instance of <see cref="IBlobReadRepository"/></returns>
        IBlobReadRepository GetBlobReadRepository();
        
        /// <summary>
        /// Get a publication repository from the session 
        /// </summary>
        /// <returns>An instance of <see cref="IPublicationReadRepository"/></returns>
        IPublicationReadRepository GetPublicationReadRepository();
        
        /// <summary>
        /// Get a tag repository from the session 
        /// </summary>
        /// <returns>An instance of <see cref="ITagReadRepository"/></returns>
        ITagReadRepository GetTagReadRepository();
        
        /// <summary>
        /// Get a source repository from the session 
        /// </summary>
        /// <returns>An instance of <see cref="ISourceReadRepository"/></returns>
        ISourceReadRepository GetSourceReadRepository();
    }
}
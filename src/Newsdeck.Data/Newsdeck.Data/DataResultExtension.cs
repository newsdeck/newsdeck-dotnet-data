using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Newsdeck.Data.Entity;
using Newsdeck.Data.Order;

namespace Newsdeck.Data
{
    /// <summary>
    /// <see cref="IDataResult{TContract,TIdentifier}"/> extensions methods
    /// </summary>
    public static class DataResultExtension
    {
        /// <summary>
        /// <see cref="IDataResult{TContract,TIdentifier}.AddFilter"/> shortcut builder method
        /// </summary>
        /// <param name="dataResult"></param>
        /// <param name="filter"></param>
        /// <typeparam name="TContract"></typeparam>
        /// <typeparam name="TIdentifier"></typeparam>
        /// <returns></returns>
        public static IDataResult<TContract, TIdentifier> FilterBy<TContract, TIdentifier>(
            this IDataResult<TContract, TIdentifier> dataResult,
            IFilter filter)
        where TContract : IBaseEntity<TIdentifier>
        {
            dataResult.AddFilter(filter);
            return dataResult;
        }
        
        /// <summary>
        /// <see cref="IDataResult{TContract,TIdentifier}.AddFilter"/> shortcut builder method
        /// </summary>
        /// <param name="dataResult"></param>
        /// <param name="filters"></param>
        /// <typeparam name="TContract"></typeparam>
        /// <typeparam name="TIdentifier"></typeparam>
        /// <returns></returns>
        public static IDataResult<TContract, TIdentifier> FilterBy<TContract, TIdentifier>(
            this IDataResult<TContract, TIdentifier> dataResult,
            IEnumerable<IFilter> filters)
        where TContract : IBaseEntity<TIdentifier>
        {
            foreach (var filter in filters)
            {
                dataResult.AddFilter(filter);
            }
            return dataResult;
        }
        
        /// <summary>
        /// <see cref="IDataResult{TContract,TIdentifier}.AddFilter"/> shortcut builder method
        /// </summary>
        /// <param name="dataResult"></param>
        /// <param name="filters"></param>
        /// <typeparam name="TContract"></typeparam>
        /// <typeparam name="TIdentifier"></typeparam>
        /// <returns></returns>
        public static IDataResult<TContract, TIdentifier> FilterBy<TContract, TIdentifier>(
            this IDataResult<TContract, TIdentifier> dataResult,
            params IFilter[] filters)
        where TContract : IBaseEntity<TIdentifier>
        {
            foreach (var filter in filters)
            {
                dataResult.AddFilter(filter);
            }
            return dataResult;
        }
        
        /// <summary>
        /// <see cref="IDataResult{TContract,TIdentifier}.SetLimit"/> shortcut builder method
        /// </summary>
        /// <param name="dataResult"></param>
        /// <param name="limit"></param>
        /// <typeparam name="TContract"></typeparam>
        /// <typeparam name="TIdentifier"></typeparam>
        /// <returns></returns>
        public static IDataResult<TContract, TIdentifier> Limit<TContract, TIdentifier>(
            this IDataResult<TContract, TIdentifier> dataResult,
            int? limit)
        where TContract : IBaseEntity<TIdentifier>
        {
            dataResult.SetLimit(limit);
            return dataResult;
        }
        
        /// <summary>
        /// <see cref="IDataResult{TContract,TIdentifier}.AddOrder"/> shortcut builder method
        /// using a <see cref="DescOrder{TContract}"/> IOrder implementation
        /// </summary>
        /// <param name="dataResult"></param>
        /// <param name="navigationPropertyPath"></param>
        /// <typeparam name="TContract"></typeparam>
        /// <typeparam name="TIdentifier"></typeparam>
        /// <returns></returns>
        public static IDataResult<TContract, TIdentifier> OrderDesc<TContract, TIdentifier>(
            this IDataResult<TContract, TIdentifier> dataResult,
            Expression<Func<TContract, object>> navigationPropertyPath)
        where TContract : IBaseEntity<TIdentifier>
        {
            dataResult.AddOrder(new DescOrder<TContract>(navigationPropertyPath));
            return dataResult;
        }
        
        /// <summary>
        /// <see cref="IDataResult{TContract,TIdentifier}.AddOrder"/> shortcut builder method
        /// using a <see cref="AscOrder{TContract}"/> IOrder implementation
        /// </summary>
        /// <param name="dataResult"></param>
        /// <param name="navigationPropertyPath"></param>
        /// <typeparam name="TContract"></typeparam>
        /// <typeparam name="TIdentifier"></typeparam>
        /// <returns></returns>
        public static IDataResult<TContract, TIdentifier> OrderAsc<TContract, TIdentifier>(
            this IDataResult<TContract, TIdentifier> dataResult,
            Expression<Func<TContract, object>> navigationPropertyPath)
        where TContract : IBaseEntity<TIdentifier>
        {
            dataResult.AddOrder(new AscOrder<TContract>(navigationPropertyPath));
            return dataResult;
        }
        
    }
}
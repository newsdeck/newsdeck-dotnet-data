using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data.Entity;

namespace Newsdeck.Data.Repository
{
    /// <summary>
    /// Base read repository
    /// Holds basic methods for every read repository
    /// </summary>
    /// <typeparam name="TContract"></typeparam>
    /// <typeparam name="TIdentifier"></typeparam>
    public interface IBaseReadRepository<TContract, TIdentifier> 
        where TContract 
        : IBaseEntity<TIdentifier>
    {
        /// <summary>
        /// current <see cref="IReadSession"/> access 
        /// </summary>
        IReadSession Session { get; }
        
        /// <summary>
        /// Return the given <T/> using it id
        /// </summary>
        /// <returns></returns>
        Task<TContract> GetWithIdAsync(TIdentifier id, CancellationToken cancellationToken);
        
        /// <summary>
        /// Get the entities as an <see cref="IDataResult{TContract, TIdentifier}"/> implementation
        /// </summary>
        /// <returns></returns>
        IDataResult<TContract, TIdentifier> Get();
    }
}
using Newsdeck.Data.Data;

namespace Newsdeck.Data.Model
{
    /// <summary>
    /// Edit publication entity model
    /// </summary>
    public interface IEditPublicationModel : IEditBaseModel, IPublicationData
    {
        
    }
}
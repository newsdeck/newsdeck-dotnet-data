using Newsdeck.Data.Filter;

namespace Newsdeck.Data
{
    /// <summary>
    /// IOrder interface method represent a contract for <see cref="Newsdeck.Data.IDataResult{TContract, TIdentifier}"/>
    /// Custom implementation can be made, but most of the classic case are covered in this package
    /// <see cref="Newsdeck.Data.Filter.AndFilter"/>, <see cref="EqualFilter{TContract}"/>, etc
    /// </summary>
    public interface IFilter
    {
    }
}
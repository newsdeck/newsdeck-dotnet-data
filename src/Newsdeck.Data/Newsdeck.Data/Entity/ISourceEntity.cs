using System.Collections.Generic;
using Newsdeck.Data.Data;

namespace Newsdeck.Data.Entity
{
    /// <summary>
    /// Source entity
    /// </summary>
    public interface ISourceEntity : IBaseEntity<long>, ISourceData
    {
        
        /// <summary>
        /// Linq access to Publications from <see cref="IPublicationData.SourceIdentifier"/>
        /// </summary>
        IEnumerable<IPublicationEntity> Publications { get; }
        
        /// <summary>
        /// Linq access to SourceIcon from <see cref="ISourceData.SourceIconIdentifier"/>
        /// </summary>
        IBlobEntity SourceIcon { get; }
    }
}
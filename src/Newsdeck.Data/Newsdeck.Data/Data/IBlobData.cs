namespace Newsdeck.Data.Data
{
    /// <summary>
    /// Blob Data
    /// </summary>
    public interface IBlobData
    {
        /// <summary>
        /// Original Url
        /// </summary>
        string OriginalUrl { get; }
        
        /// <summary>
        /// Content type
        /// </summary>
        string ContentType { get; }
        
        /// <summary>
        /// Json data
        /// Contains data not used for dataset querying
        /// but used later on in business stack
        /// </summary>
        string JsonData { get; }
        
        /// <summary>
        /// Handler json data
        /// Contains data not used for dataset querying
        /// but used later on in business stack
        /// </summary>
        string HandlerJsonData { get; }
    }
}
using Newsdeck.Data.Data;

namespace Newsdeck.Data.Entity
{
    /// <summary>
    /// Blob entity
    /// </summary>
    public interface IBlobEntity : IBaseEntity<long>, IBlobData
    {
        
    }
}
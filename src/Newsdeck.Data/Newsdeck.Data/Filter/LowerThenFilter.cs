using System;
using System.Linq.Expressions;

namespace Newsdeck.Data.Filter
{
    /// <summary>
    /// Lower filter
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class LowerThenFilter<T> : PropertyFilter<T>
    {
        /// <summary>
        /// Expose the filter value
        /// </summary>
        public IComparable Value => _value;
        private readonly IComparable _value;

        /// <summary>
        /// Default constructor for LowerThenFilter
        /// </summary>
        /// <param name="navigationPropertyPath">Access to the property</param>
        /// <param name="value">Filter value</param>
        public LowerThenFilter(Expression<Func<T, object>> navigationPropertyPath,
            IComparable value)
            : base(navigationPropertyPath)
        {
            _value = value;
        }
    }
}
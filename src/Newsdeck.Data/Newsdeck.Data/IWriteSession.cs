using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data
{
    /// <summary>
    /// Write session represent a database session where we can apply modification to the dataset
    /// We provide method to retrieve writes repository and expose two methods :
    /// <see cref="CommitAsync"/> and <see cref="RollbackAsync"/>
    /// </summary>
    /// <inheritdoc cref="IReadSession"/>
    public interface IWriteSession : IReadSession
    {
        /// <summary>
        /// Get a blob repository from the session 
        /// </summary>
        /// <returns>An instance of <see cref="IBlobWriteRepository"/></returns>
        IBlobWriteRepository GetBlobWriteRepository();
        
        /// <summary>
        /// Get a publication repository from the session 
        /// </summary>
        /// <returns>An instance of <see cref="IPublicationWriteRepository"/></returns>
        IPublicationWriteRepository GetPublicationWriteRepository();
        
        /// <summary>
        /// Get a tag repository from the session 
        /// </summary>
        /// <returns>An instance of <see cref="ITagWriteRepository"/></returns>
        ITagWriteRepository GetTagWriteRepository();
        
        /// <summary>
        /// Get a source repository from the session 
        /// </summary>
        /// <returns>An instance of <see cref="ISourceWriteRepository"/></returns>
        ISourceWriteRepository GetSourceWriteRepository();

        /// <summary>
        /// Commit all the modifications made during the session scope
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task CommitAsync(CancellationToken cancellationToken);
        
        /// <summary>
        /// Rollback all the modifications made during the session scope
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task RollbackAsync(CancellationToken cancellationToken);
    }
}
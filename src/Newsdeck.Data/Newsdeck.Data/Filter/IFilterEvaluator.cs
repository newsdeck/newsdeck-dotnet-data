using System;
using Newsdeck.Data.Order;

namespace Newsdeck.Data.Filter
{
    /// <summary>
    /// IFilterEvaluator contract
    /// used to enforce default implementation of the IDataResult implementation
    /// to have basic needs implemented
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TContract"></typeparam>
    public interface IFilterEvaluator<T, TContract>
    {
        /// <summary>
        /// Register a new filter evaluator 
        /// </summary>
        /// <param name="r"></param>
        /// <typeparam name="TFilter"></typeparam>
        void RegisterFilterEvaluator<TFilter>(Func<TFilter, T> r)
            where TFilter : IFilter;

        /// <summary>
        /// Register a new order evaluator 
        /// </summary>
        /// <param name="r"></param>
        /// <typeparam name="TOrder"></typeparam>
        void RegisterOrderEvaluator<TOrder>(Func<TOrder, T> r)
            where TOrder : IOrder;

        /// <summary>
        /// Define method signature to handle AndFilter
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        T EvaluateAnd(AndFilter f);
        
        /// <summary>
        /// Define method signature to handle EqualFilter
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        T EvaluateEqualFilter(EqualFilter<TContract> f);
        
        /// <summary>
        /// Define method signature to handle DifferentFilter
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        T EvaluateNotEqualFilter(DifferentFilter<TContract> f);
        
        /// <summary>
        /// Define method signature to handle InFilter
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        T EvaluateIn(InFilter<TContract> f);
        
        /// <summary>
        /// Define method signature to handle LowerOrEqualThenFilter
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        T EvaluateLowerOrEqualThenFilter(LowerOrEqualThenFilter<TContract> f);
        
        /// <summary>
        /// Define method signature to handle LowerThenFilter
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        T EvaluateLowerThenFilter(LowerThenFilter<TContract> f);
        
        /// <summary>
        /// Define method signature to handle GreaterOrEqualThenFilter
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        T EvaluateGreaterOrEqualThenFilter(GreaterOrEqualThenFilter<TContract> f);
        
        /// <summary>
        /// Define method signature to handle GreaterThenFilter
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        T EvaluateGreaterThenFilter(GreaterThenFilter<TContract> f);
        
        
        /// <summary>
        /// Define method signature to handle DescOrder
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        T EvaluateDescOrder(DescOrder<TContract> o);
        
        /// <summary>
        /// Define method signature to handle AscOrder
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        T EvaluateAscOrder(AscOrder<TContract> o);
    }
}
using Newsdeck.Data.Data;

namespace Newsdeck.Data.Model
{
    /// <summary>
    /// Create publication entity model
    /// </summary>
    public interface ICreatePublicationModel : ICreateBaseModel, IPublicationData
    {
        
    }
}
using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data.Entity;
using Newsdeck.Data.Repository;

namespace Newsdeck.Data.BehaviorTest
{
    public class TestBlobReadRepository : IBlobReadRepository
    {
        private readonly TestReadSession _readSession;
        
        public TestBlobReadRepository(TestReadSession readSession)
        {
            _readSession = readSession;
        }

        IReadSession IBaseReadRepository<IBlobEntity, long>.Session => _readSession; 
        
        
        public Task<IBlobEntity> GetWithIdAsync(long id, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public IDataResult<IBlobEntity, long> Get()
        {
            throw new System.NotImplementedException();
        }
    }
}